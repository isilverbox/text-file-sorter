package service.sorter;

import org.junit.Before;
import org.junit.Test;
import service.sorter.FileSorterImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileSorterImplTest {
	FileSorterImpl fileSorterImpl;

	@Before
	public void setUp() throws Exception {
		fileSorterImpl = new FileSorterImpl(new File("data/text/text_file.txt"));
	}

	@Test
	public void sort() throws IOException {
		fileSorterImpl.splitIntoLines(100);
		fileSorterImpl.sort();
	}
}