package service.generator;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileGeneratorTest {

	@Test
	public void generateRandomTextFile() throws IOException {
		FileGenerator fileGenerator = new FileGenerator(new RandomStringGeneratorImpl(0, 100));
		fileGenerator.setLinesCount(1000);
		fileGenerator.generateTextFile();
	}

	/*@Test
	public void dirDelete() throws IOException {
		File file = new File("data/text/temp");
		System.out.println(file.exists());
		Files.delete(Path.of(file.getAbsolutePath()));
	}*/

	@Test
	public void nullTest() {
		List<String> list = new ArrayList<>();
		list.add("");
		list.add(null);
		System.out.println(list.size());
	}
}