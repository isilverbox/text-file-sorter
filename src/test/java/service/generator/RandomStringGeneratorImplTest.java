package service.generator;

import org.junit.Test;
import service.generator.RandomStringGeneratorImpl;

public class RandomStringGeneratorImplTest {

	@Test
	public void nextLine() {
		RandomStringGeneratorImpl stringGenerator = new RandomStringGeneratorImpl(10, 100);
		for (int i = 0; i< 10; i++) {
			System.out.println(stringGenerator.nextLine());
		}
	}
}