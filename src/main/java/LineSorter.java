import service.sorter.LineSorterService;

import java.io.File;

public class LineSorter {

	public static void main(String[] args) {
		LineSorterService lineSorterService = new LineSorterService(new File("data/text/text_file.txt"));
		lineSorterService.sort();
	}
}
