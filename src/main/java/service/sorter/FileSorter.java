package service.sorter;

import java.io.IOException;

public interface FileSorter {
	void sort() throws IOException;
	void splitIntoLines(int lines);
}
