package service.sorter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class FileSorterImpl implements FileSorter {

	private final File currentFile;
	private final Path tempFolder;
	private final File resultFile;
	private int splitMaxLines = 200000;
	private final List<File> fileList = new ArrayList<>();

	public FileSorterImpl(File currentFile) {
		this.currentFile = currentFile;
		this.tempFolder = currentFile.toPath().getParent().resolve("temp");
		this.resultFile = currentFile.toPath().getParent().resolve("sorted_" + currentFile.getName()).toFile();
	}

	public void sort() throws IOException {
		splitFile();
		sortToResultFile();
		deleteTempFolder();
	}

	@Override
	public void splitIntoLines(int lines) {
		splitMaxLines = lines;
	}

	private void splitFile() throws IOException {
		if (!Files.exists(tempFolder)) Files.createDirectory(tempFolder);

		int namePrefix = 0;
		int lineCounter = 0;

		List<String> lineList = new ArrayList<>();

		try (BufferedReader bufferedReader = new BufferedReader(Files.newBufferedReader(currentFile.toPath()))) {
			String line;

			while ((line = bufferedReader.readLine()) != null) {
				lineList.add(line);
				lineCounter++;

				if (lineCounter == splitMaxLines) {
					lineCounter = 0;
					sortAndWrite(lineList, namePrefix++);
					lineList.clear();
				}
			}

			// Запишем оставшееся
			sortAndWrite(lineList, namePrefix);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void sortAndWrite(List<String> lineList, int fileNamePrefix) throws IOException {
		if (lineList.isEmpty()) return;
		lineList.sort(String.CASE_INSENSITIVE_ORDER);
		File tempFile = new File(tempFolder.toString(), fileNamePrefix + "_" + currentFile.getName());
		fileList.add(tempFile);
		Files.write(tempFile.toPath(), lineList, Charset.defaultCharset());
	}

	private void sortToResultFile() throws IOException {
		int readersCount = fileList.size();

		// Массив строк равный кол-ву файлов
		List<String> stringList = new ArrayList<>(readersCount);

		// Массив читалок равный кол-ву файлов
		List<BufferedReader> bufferedReaders = new ArrayList<>(readersCount);

		// Инициализация читалок и первоначальное заполнение строк
		for (int i = 0; i < readersCount; i++) {
			bufferedReaders.add(i, new BufferedReader(Files.newBufferedReader(fileList.get(i).toPath())));
			stringList.add(i, bufferedReaders.get(i).readLine());
		}
		// Запись в result файл
		BufferedWriter bufferedWriter = new BufferedWriter(Files.newBufferedWriter(resultFile.toPath()));

		String line;
		int f;

		while (!bufferedReaders.isEmpty()) {

			f = minLineIndex(stringList);
			bufferedWriter.write(stringList.get(f));
			bufferedWriter.newLine();

			if ((line = bufferedReaders.get(f).readLine()) != null) {
				stringList.set(f, line);
			} else {
				bufferedReaders.get(f).close();
				bufferedReaders.remove(f);
				stringList.remove(f);
			}
		}
		bufferedWriter.close();
	}


	/**
	 * Получить индекс текста ближе к началу алфавита
	 *
	 * @param strings список строк
	 * @return индекс
	 */
	private int minLineIndex(List<String> strings) throws NoSuchElementException {
		return strings.indexOf(Collections.min(strings, String.CASE_INSENSITIVE_ORDER));
	}

	/**
	 * Удаление временных файлов
	 */
	private void deleteTempFolder() {
		try {
			Files.walk(tempFolder)
					.sorted(Comparator.reverseOrder())
					.map(Path::toFile)
					.forEach(File::delete);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<File> getFileList() {
		return fileList;
	}
}
