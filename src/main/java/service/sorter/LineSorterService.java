package service.sorter;

import java.io.File;
import java.io.IOException;

public class LineSorterService {
	private FileSorter fileSorter;

	public LineSorterService(File file) {
		this.fileSorter = new FileSorterImpl(file);
	}

	public void setFileSorter(FileSorter fileSorter) {
		this.fileSorter = fileSorter;
	}

	public void sort() {
		try {
			fileSorter.sort();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
