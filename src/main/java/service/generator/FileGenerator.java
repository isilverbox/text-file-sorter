package service.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileGenerator {
	private final File file = new File("data/text/text_file.txt");
	private int lines = 10000000;
	private final RandomStringGenerator stringGenerator;

	public FileGenerator(int minLineLength, int maxLineLength) {
		this.stringGenerator = new RandomStringGeneratorImpl(minLineLength, maxLineLength);
	}

	public FileGenerator(RandomStringGenerator stringGenerator) {
		this.stringGenerator = stringGenerator;
	}

	public void generateTextFile() throws IOException {
		Files.createDirectories(file.toPath().getParent());
		try (BufferedWriter bufferedWriter = Files.newBufferedWriter(file.toPath())) {
			for (int i = 0; i < lines; i++) {
				bufferedWriter.write(stringGenerator.nextLine());
				bufferedWriter.newLine();
			}
		}
	}

	public File getFile() {
		return file;
	}

	public int getLines() {
		return lines;
	}

	public void setLinesCount(int lines) {
		this.lines = lines;
	}

	public RandomStringGenerator getStringGenerator() {
		return stringGenerator;
	}
}
