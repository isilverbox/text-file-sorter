package service.generator;

import java.util.Random;

public class RandomStringGeneratorImpl implements RandomStringGenerator {
	private final int minLineLength;
	private final int maxLineLength;
	private static final char[] subset = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
	private final int subsetLength = subset.length;
	private final Random random = new Random();


	public RandomStringGeneratorImpl(int minLineLength, int maxLineLength) throws IllegalArgumentException {
		if (minLineLength < 0 || maxLineLength < 1) {
			throw new IllegalArgumentException("minLineLength must be positive or zero and maxLineLength must be > 0");
		}
		this.minLineLength = minLineLength;
		this.maxLineLength = maxLineLength + 1;
	}

	public RandomStringGeneratorImpl(int maxLineLength) throws IllegalArgumentException {
		this(0, maxLineLength);
	}

	public String nextLine() {
		int charsCount = random.nextInt(maxLineLength) + minLineLength;
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < charsCount; i++) {
			result.append(subset[random.nextInt(subsetLength)]);
		}

		return result.toString();
	}
}
