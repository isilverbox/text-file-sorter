package service.generator;

public interface RandomStringGenerator {
	String nextLine();
}
